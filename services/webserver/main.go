package main

import (
    "github.com/labstack/echo"
    "github.com/labstack/echo/middleware"
    "net/http"
)

func main() {

    // Echo instance
    e := echo.New()

    e.Use(middleware.Logger())
    e.Use(middleware.Secure())
    e.Use(middleware.Recover())

    e.Static("/", "assets")

    echo.NotFoundHandler = func(c echo.Context) error {
        // render your 404 page
        return c.String(http.StatusNotFound, "not found page")
    }
    // Start server
    e.Logger.Fatal(e.Start(":8080"))
}
