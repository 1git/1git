# Webserver
The webserver is the frontend that graphically displays 1git and uses the [Echo Framework](https://github.com/labstack/echo) as a base.

## Static file serving
The webserver is configured to map all request to the path `/*` to the assets folder. Making a request to `/hello.txt` serves the file `assets/hello.txt`.

## Security
Currently only the default security settings of echo's [Secure Middleware](https://echo.labstack.com/middleware/secure) is used. This provides protection against cross-site scripting (XSS) attack, content type sniffing, clickjacking, insecure connection and other code injection attacks. 