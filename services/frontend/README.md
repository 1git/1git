# 1git Frontend

This is a Vue.js Progressive Web App and the main frontend for 1git.

## Requirements

You need to install Node.JS & npm. At least version 9.4.0 or higher.

## Getting Started

While developing you can serve this page by running:

```sh
npm run server
```

To build this app for production run

```sh
npm run build.
```

This command also runs linters as well as other code quality tools.
