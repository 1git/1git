import Vue from 'vue';
import Router from 'vue-router';
import ComingSoon from './views/ComingSoon.vue';

import Home from './views/Home.vue';
import Documentation from './components/Documentation.vue';
import GettingStarted from './components/GettingStarted.vue';
import Pricing from './components/Pricing.vue';
import Support from './components/Support.vue';
import About from './views/About.vue';

Vue.use(Router);

export default new Router({
  scrollBehavior (to, from, savedPosition) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ x: 0, y: 0 })
      }, 500)
    })
  },
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home,
      children: [
        {
          path: '/getting-started',
          name: 'getting-started',
          component: GettingStarted,
        },
        {
          path: '/home',
          name: 'home',
          component: About,
        },
        {
          path: '/pricing',
          component: Pricing,
        },
        {
          path: '/support',
          component: Support,
        },
        {
          path: '/documentation',
          name: 'documentation',
          component: Documentation,
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/',
      name: 'coming-soon',
      component: ComingSoon,
    },
  ],
});
