export default {
  state: {
    loading: false,
    repo_name: null,
    user_name: null,
    email: null,
    email_code: null,
    step: 'repo',
  },
  mutations: {
    startLoading (state) {
      state.loading = true;
    },
    stopLoading (state) {
      state.loading = false;
    },
    pickRepoName (state, name) {
      state.loading = false;
      state.repo_name = name;
      state.step = 'username';
    },
    pickUserName (state, name) {
      state.loading = false;
      state.user_name = name;
      state.step = 'email';
    },
    pickEmail (state, email) {
      state.loading = false;
      state.email = email;
      state.step = 'email-confirmation';
    },
    confirmEmail (state, code) {
      state.loading = false;
      state.email = code;
      state.step = 'password';
    },
    finishRegistration (state) {
      state.loading = false;
      state.repo_name = null;
      state.user_name = null;
      state.step = 'finished';
    },
  },
  actions: {
    pickRepoName ({ commit }, name) {
      return new Promise((resolve, reject) => {
        commit('startLoading')
        setTimeout(() => {
          commit('pickRepoName', name)
          resolve()
        }, 1000)
      });
    },
    pickUserName ({ commit }, name) {
      return new Promise((resolve, reject) => {
        commit('startLoading')
        setTimeout(() => {
          commit('pickUserName', name)
          resolve()
        }, 1000)
      });
    },
    pickEmail ({ commit }, name) {
      return new Promise((resolve, reject) => {
        commit('startLoading')
        setTimeout(() => {
          commit('pickEmail', name)
          resolve()
        }, 1000)
      });
    },
    confirmEmail ({ commit }, name) {
      return new Promise((resolve, reject) => {
        commit('startLoading')
        setTimeout(() => {
          commit('confirmEmail', name)
          resolve()
        }, 1000)
      });
    },
    finishRegistration ({ commit }, name) {
      return new Promise((resolve, reject) => {
        commit('startLoading')
        setTimeout(() => {
          commit('finishRegistration', name)
          resolve()
        }, 1000)
      });
    },
  },
};
