import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import registration from './registration'
export default new Vuex.Store({
  modules: {
    registration
  },
});
