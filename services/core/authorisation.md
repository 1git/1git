# Authorisiation Model
This document describes the implemented authorisation model and how services can interact with it. 

# core
The service responsible for central authorisation management is `core`. It offers API endpoints for the frontend to determine wheter an actor is allowed to perform an action or not. It also offers an API endpoint for services to check wheter an actor trying to perform an action is authorised to do so.

In the context of `core` all things that can perfom an action like push, pull, build, ... are seen as an `actor`. Each actor has rights for different actions and ressources. Ressources are for example the permission to read/write specific files, folders or repositories. 

## Persisting data
For data persistence a postgres database is used.

## Service endpoints
tbd

## Frontend endpoints
tbd