package main

import (
    "fmt"
    "gitlab.com/1git/1git/pkg/helloworldwriter"
)

func main() {
    var helloWorldWriter = helloworldwriter.HelloWorldWriter{}
    fmt.Println(helloWorldWriter.GetHelloWorld())
}
