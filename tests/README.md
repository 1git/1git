# 1git Web UI E2E Tests

These are the 1git E2E Tests. The `docker-compose.yml` is used to run the E2E tests in our CI/CD pipe.

## How do I run these tests?

You need to have Docker as well as Docker Compose installed!

We are using our Cypress test image. You can find more information about this image [**here**](../docker/cypress/README.md).

In our CI/CD pipe we are setting an `IMAGE_TAG` variable based on the current branch. Locally you can set this variable to pick against which images you want the tests to run against. To run the tests against the current release version set it to `latest`:

```sh
export IMAGE_TAG=latest
```

Now you can start the 1git services locally like this:

```sh
docker-compose -f docker-compose.yml up -d
```

Once the services are up and running you can run the tests like this:

```sh
docker-compose -f docker-compose.yml run cypress service run --config baseUrl=http://webserver:1323
```

<sub>**NOTE**: The commands above assume your working dir is in the same folder as this readme.</sub>