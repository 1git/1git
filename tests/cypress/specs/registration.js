

describe('Registration Flow', () => {
  it('Clicks through the registration process.', () => {
    cy.visit('/#/getting-started');
    cy.get('#repo_input').type('test-repo');
    cy.contains('Next').click();
    cy.get('#user_input').type('test-name');
    cy.contains('Next').click();
    cy.get('#email_input').type('test-email@1git.io');
    cy.contains('Next').click();
    cy.get('#email_confirmation_input').type('test-confirmation-code');
    cy.contains('Next').click();
    cy.get('#password_input').type('test-password');
    cy.contains('Next').click();
  });
});
