// https://docs.cypress.io/api/introduction/api.html

describe('Coming Soon Page', () => {
  it('Visits the Coming Soon Page', () => {
    cy.visit('/');
    cy.get('h1').contains('1git');
    cy.get('h2').contains('Coming Soon!');
    cy.get('#sources-link').click();
  });
});