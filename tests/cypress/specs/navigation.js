

describe('Home Page', () => {
  it('Clicks through the home page.', () => {
    cy.visit('/#/home');
    cy.get('#drawer-button').click();
    cy.contains('Getting Started').click();
  });
});
