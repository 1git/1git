#!/bin/bash
set -e

addKey() {
  echo -n "command=\"/usr/local/bin/shell key-2\",no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty " >> /ssh/authorized_keys
  cat "${1}.pub" >> /ssh/authorized_keys
  chmod -R 700 /ssh
  chown -R 1000.1000 /ssh
}

useKey() {
  echo "Host shell" > /root/.ssh/config
  echo "  User 1git" >> /root/.ssh/config
  echo "  IdentityFile ${1}" >> /root/.ssh/config
  chmod -R 700 /root/.ssh
}

sleep 2
mkdir /keys
mkdir /root/.ssh


ssh-keygen -t ed25519 -f /keys/test_ed25519 < /dev/null
ssh-keygen -t rsa -b 4096 -f /keys/test_rsa < /dev/null
ssh-keygen -t ed25519 -f /keys/test_broken < /dev/null

echo "" > /ssh/authorized_keys
addKey "/keys/test_ed25519"
addKey "/keys/test_rsa"

echo "Trusting host keys"
ssh-keyscan shell >> /root/.ssh/known_hosts
chmod -R 700 /root/.ssh

# Test clone with ed25519 key with empty repo
echo "Using key /keys/test_ed25519"
useKey "/keys/test_ed25519"
cd /
git clone 1git@shell:/ test
git config --global user.email "e2e-test@example.com"
git config --global user.name "E2E Test"
git config --global push.default simple
cd test

head /dev/urandom > ff && git add -A && git commit -m 'commit test' && git push

# Test clone witg rsa key, non empty repo
echo "Using key /keys/test_rsa"
useKey "/keys/test_rsa"
cd /

git clone 1git@shell:/ test2
cd test2
head /dev/urandom > ff && git add -A && git commit -m 'commit test2' && git push

# Test pull
echo "Using key /keys/test_ed25519"
useKey "/keys/test_ed25519"
cd /test
git pull


# Test pull with unauthorized key
echo "Using key /keys/test_broken"
useKey "/keys/test_broken"
cd /test
git pull && exit 1 || exit 0
