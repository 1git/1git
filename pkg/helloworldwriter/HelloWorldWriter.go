package helloworldwriter
// HelloWorldWriter is a testing struct
type HelloWorldWriter struct {

}
// GetHelloWorld always returns the string "Hello World"
func (h *HelloWorldWriter) GetHelloWorld() string {
    return "Hello World"
}