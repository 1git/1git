# Bug Report

[//]: # (Relevant Emojis go here. Emoji Severity Scale:)
[//]: # (:shrug: -> :ok_hand: -> :astonished: -> :cold_sweat: -> :fire_engine: -> :fire: -> :bomb:)

## Brief summary
[//]: # (Add a brief description what this bug is about.)
[//]: # (For example where it happens and which use cases it affects.)

Bug happens here: [URL]

Affected use cases:
 - Use case 1
 - Use case 2

## Steps to reproduce
[//]: # (Detailed description of the steps taken to reproduce the error.)

1. Do this
2. Click here
3. ???
4. Enjoy your error.

## Desired behavior
[//]: # (Detailed description of the desired behavior or what you expect to/think should happen.)

## Actual behavior
[//]: # (Detailed description of the error itself and what effects it has.)