[![pipeline status](https://gitlab.com/1git/1git/badges/master/pipeline.svg)](https://gitlab.com/1git/1git/commits/master)
[![coverage report](https://gitlab.com/1git/1git/badges/master/coverage.svg)](https://gitlab.com/1git/1git/commits/master)

# 1git
1git is a code management and build system for mono repos. It is designed to support your complete development lifecycle from building to deploying your code to production.

## Current State
This project is in a very early alpha stage. It is not recommended to use it in production. There can be major design changes at any time.

## Goals
 - **Configuration is part of the source code**: In 1git every setting and even user management is part of the source code. Everything can be managed through GIT.
 - **Opinionated Workflow**: 1git is intended to solve the problems that matter most in a monorepo. It enforces best practices which enables the automation of most CI/CD related problems usually encountered in other CI/CD solutions. 
 - **Automated incremental CI/CD pipeline**: 1git takes care of generating an optimal and fully incremental build and delivery pipeline for projects in your monorepo.
 - **Enterprise Ready**: Wheter you use the cloud service or host a 1git instance on premise you can expect easy collaboration and high availability that scales seamlessly even for large repos and huge teams.
 - **Security by design**: 1git was built from the ground up with security in mind. You can expect frequent updates and airtight access management through ssh.

## Roadmap
- [ ] Basic GIT Server
- [ ] Write permissions (down to files) using GIT hooks.
- [ ] Read permissions at least on repository level.
- [ ] Users can interact with git repositories through the normal git client - as long as they have the permissions to do so.
- [ ] Creates intial admin user on first time setup.
- [ ] Users can register in a web UI
- [ ] Users can manage their repository in a Web UI. Only simple actions like:
    - [ ] Create a repository
    - [ ] Delete a repository
    - [ ] View a repository
    - [ ] Add/remove users from/to their repository

## Architecture

Services:
 - 1git Guard Service
     - Handles GIT hooks and GIT integrations
 - Permission Service
     - Contains user data base and permission (for example read/write to repositories or even folders.)
 - Web Servers
     - Serves content like the web frontend
 - NGINX
     - Reverseproxy and API Gateway

## Setup
Since Go expects all Go related projects to be in `$GOPATH` it is recommended to have the local developement repository in `$GOPATH/src`. If you have a running go environemnt the easiest way to set up the local developement environment is by executing `go get gitlab.com/1git/1git`. This clones the repository to `$GOPATH/src/gitlab.com/1git/1git`.

The dependency management is done by [dep](https://github.com/golang/dep#dep) which can be installed in different ways. 
## Repository structure
The repository structure is based on a [project-structure-template](https://github.com/golang-standards/project-layout). This describes a best practice when working with multiple go packages in a single repository. 

### cmd/
This directory contains all cli clients used to communicate with 1git services. 

### pkg/
This directory contains packages that can safely be referenced by other go projects.

### services/
This directory contains services that are needed for 1git to work.

### vendor/
This directory contains external dependencies and is managed by `dep`.

## How to Contribute
Pick an issue, push a MR and instantly become a member of our contributors community.

## License
GNU General Public License v3.0

See [LICENSE](LICENSE) to see the full text.
