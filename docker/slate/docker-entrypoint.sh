#!/bin/sh -e

if [ "${1}" = "service" ]; then
  exec bundle exec middleman server --watcher-force-polling
elif [ "${1}" = "build" ]; then
  exec bundle exec middleman build
else
  exec "${@}"
fi
