# 1git Slate Docker Container

This is the Docker container used to generate Slate documentation for 1git. For more information about Slate click [**here**](https://github.com/lord/slate).

## How do I use it?

To edit or preview the documentation just run this command:

```shell
docker-compose -f docker-compose.yml up
```

<sub>**NOTE**: The command above assumes your working dir is in the same folder as this readme.</sub>

As soon as the container is up and running you can open http://localhost:4567 in your browser and preview the documentation. Now you can edit the markdown file in the `sources` folder and after a reload the page should contain your updated documentation.

## How do I deploy it?

We deploy the static html generated by slate directly instead of hosting this slate container. To build static html for the current documentation run this command:

```shell
docker-compose -f docker-compose.yml run app build
```

<sub>**NOTE**: The command above assumes your working dir is in the same folder as this readme.</sub>

This will generate the static html and output it into the `build` folder next to this readme file.