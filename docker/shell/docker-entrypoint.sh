#!/bin/sh -e

if [ "${1}" = "run" ]; then
  [ -f /etc/ssh/host_keys/ssh_host_ed25519_key ] || ssh-keygen -t ed25519 -f /etc/ssh/host_keys/ssh_host_ed25519_key < /dev/null
  [ -f /etc/ssh/host_keys/ssh_host_rsa_key ] || ssh-keygen -t rsa -b 4096 -f /etc/ssh/host_keys/ssh_host_rsa_key < /dev/null

  [ -d /var/1git/repo/hooks ] || { 
    ls -hal /var/1git/repo
    cd /var/1git/repo
    git init --bare
    chown -R 1git.1git /var/1git/repo
  }
  exec /usr/sbin/sshd -D -e
else
  exec "$@"
fi
