#!/bin/sh -e

if [ "${1}" = "run" ]; then
  exec "/opt/app/server"
else
  exec "$@"
fi
