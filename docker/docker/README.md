# 1git Docker-in-Docker Image

This is a Docker image which enables you to run Docker inside a container. It comes preinstalled with `docker` as well as `docker-compose`.

## How do I build it?

No additional configuration or dependencies beside Docker are required, you can just build it using `docker build`:

```sh
docker build -t registry.gitlab.com/1git/1git/docker .
```

<sub>**NOTE**: The command above assumes your working dir is in the same folder as this readme.</sub>

## How do I use it?

This image is mainly intended to be used inside CI/CD pipelines. Locally you can start it for example like this:

```sh
docker run -ti \
       -v /var/run/docker.sock:/var/run/docker.sock \
       registry.gitlab.com/1git/1git/docker /bin/sh
```

By sharing the Docker socket with the container you can use Docker inside the container, however be aware that any container you spawn are running alongside the docker-in-docker container, not inside it. For example running `docker ps` inside the container would also include the container itself:

```
○ → docker run -ti -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/1git/1git/docker /bin/sh
/ # docker ps
CONTAINER ID        IMAGE                                                                                                 COMMAND                  CREATED             STATUS              PORTS                    NAMES
e954259bd4be        registry.gitlab.com/1git/1git/docker                                                                  "docker-entrypoint.s…"   2 seconds ago       Up 1 second                                  elastic_leakey
/ #
```