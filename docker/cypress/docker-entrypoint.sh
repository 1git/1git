#!/bin/sh -e

if [ "${1}" = "service" ]; then
  shift
  exec cypress "${@}"
else
  exec "$@"
fi
