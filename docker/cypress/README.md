# 1git Cypress E2E Test Image

This is a Docker image inteded to run E2E tests against web frontends in ci/cd pipelines.

It uses the E2E testing framework Cypress. You can find more information about Cypress [**here**](https://www.cypress.io/)

## How do I build it?

No additional configuration or dependencies beside Docker are required, you can just build it using `docker build`:

```sh
docker build -t registry.gitlab.com/1git/1git/cypress .
```

<sub>**NOTE**: The command above assumes your working dir is in the same folder as this readme.</sub>

## How do I use it?

The easiest way to run e2e tests is to use `docker-compose`. To that end you need to write a `docker-compose.yml` which looks something like this:

```yaml
version: '3.6'

services:
  
  # Add one or more services you want to test here. This example only defines one. 
  # To find out how you can setup your services to work in docker compose please 
  # look at the Docker Compose documentation.
  server:
    image: my-web-server
    # This network is used to connect it to the Cypress test image
    networks:
      - cypress-server

  # Define service using this cypress test image.
  cypress:
    image: "registry.gitlab.com/1git/1git/cypress:latest"
    # This initial command to the image verifies that Cypress is useable.
    command: service verify
    # Mount your Cypress tests into the /opt/tests folder
    # The folder you mount has to contain the cypress.json
    volumes:
      - ./tests:/opt/tests
    # This network is used to connect to your server.
    networks:
      - cypress-server

# You have to list the network used above here, otherwise the services can't talk to each other.
networks:
  cypress-server:
```

Once you have a valid `docker-compose.yml` you first have to start your services using this command:

```sh
docker-compose -f docker-compose.yml up -d
```

It might take a few seconds for your services to start. Once they are ready you can run the tests like this:

```sh
docker-compose -f docker-compose.yml run cypress service run --config baseUrl=http://server:8080
```

<sub>**NOTE**: The command above assumes your server is running on port 8080.</sub>

Once the tests are done you can find any screenshots or videos cypress recorded in the videos, and screenshots folder you mounted into the container.